(define-module (ynm-core use-cases ingredients)
  #:use-module (ice-9 optargs)
  #:use-module (ynm-core entities))

(define*-public (ingredients food-base #:key label)
  (if (not label)
      food-base
      (filter
       (lambda (element)
	 (string-contains-ci (food-label element) label))
       food-base)))
