(define-module (ynm-core adapters)
  #:use-module (ice-9 textual-ports)
  #:use-module (dsv)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients)
  #:use-module (ynm-core use-cases intake)
  #:use-module (ynm-core use-cases balance)
  #:use-module (ynm-core use-cases recommendations))

(define-public build-calnut-base
(lambda (calnut-table)
  ((build-food-base calnut-builder) (calnut-read calnut-table))))

(define build-food-base
  (lambda (food-builder)
    (lambda (ressource)
      (map
       food-builder
       ressource))))

(define calnut-builder
  (lambda (entry)
    (let ([index-code 0]
	  [index-label 1]
	  [index-hypoth 2]
	  [index-nrj-kj 3]
	  [index-nrj-kcal 4]
	  [calnut-alim-tokenized (string-split (car entry) #\;)])
      (make-food
       (string->number (list-ref calnut-alim-tokenized index-code))
       (list-ref calnut-alim-tokenized index-label)
       (list-ref calnut-alim-tokenized index-hypoth)
       (make-intake 
	(string->number (list-ref calnut-alim-tokenized index-nrj-kj))
	(string->number (list-ref calnut-alim-tokenized index-nrj-kcal)))))))

(define calnut-read
  (lambda (table)
    (filter-mb-entries
     (call-with-input-string
	 (remove-useless-quotes (read-file table))
       dsv->scm))))

(define (filter-mb-entries entries)
  (filter (lambda (line) (string-contains (car line) ";MB;")) entries))

(define (remove-useless-quotes file-content)
  (let ([double-quotes #\"])
    (string-delete double-quotes file-content)))

(define (read-file filename)
  (call-with-input-file filename get-string-all))
