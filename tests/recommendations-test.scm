(define-module (tests harness-recommendations)
  #:use-module (spec)
  #:use-module (tests utils)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients)
  #:use-module (ynm-core use-cases balance)
  #:use-module (ynm-core use-cases intake)
  #:use-module (ynm-core use-cases recommendations)
  #:use-module (ynm-core adapters))

(install-spec-runner-repl)

(define-public (run-tests-recommendations)
  (describe "recommendations"
    (it "recommends nothing if balanced"
      (should=
	  '()
	(recommendations
	 (make-intake 0 0)
	 (build-calnut-base CALNUT_TABLE_TWO_ALIMS))))
    (it "recommends the food that provides the more energy (kcal)"
      (should=
	  (make-food
	   12112
	   "Abondance"
	   "MB"
	   (make-intake 1640 395))
	(recommendations
	 (make-intake -10770 -2574)
	 (build-calnut-base CALNUT_TABLE_TWO_ALIMS))))
    (it "recommends the food that provides the more energy (kcal) other food base"
      (should=
	  (make-food
	   40601
	   "Abat, cuit (aliment moyen)"
	   "MB"
	   (make-intake 682 162))
	(recommendations
	 (make-intake -10770 -2574)
	 (build-calnut-base CALNUT_TABLE_ONE_ALIM_1))))
    (it "recommends the food that provides the less energy (kcal)"
      (should=
	  (make-food
	   40601
	   "Abat, cuit (aliment moyen)"
	   "MB"
	   (make-intake 682 162))
	(recommendations
	 (make-intake 10770 2574)
	 (build-calnut-base CALNUT_TABLE_TWO_ALIMS))))))
