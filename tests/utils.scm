(define-module (tests utils))

(define-public CALNUT_TABLE_NO_ALIM "tests/test-files/calnut-table-no-alim.csv")
(define-public CALNUT_TABLE_ONE_ALIM_1 "tests/test-files/calnut-table-one-alim.csv")
(define-public CALNUT_TABLE_ONE_ALIM_2 "tests/test-files/calnut-table-one-alim-bis.csv")
(define-public CALNUT_TABLE_TWO_ALIMS "tests/test-files/calnut-table-two-alims.csv")
