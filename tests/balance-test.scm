(define-module (tests harness-balance)
  #:use-module (spec)
  #:use-module (tests utils)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients)
  #:use-module (ynm-core use-cases balance)
  #:use-module (ynm-core use-cases intake)
  #:use-module (ynm-core adapters))

(install-spec-runner-repl)

(define-public (run-tests-balance)
  (describe "balance"
    (it "compares null intake to arbitrary profile"
      (should=
	  (make-intake -10770 -2574)
	(balance
	 (make-profile 'man 32 69 22)
	 (make-intake 0 0))))
    (it "compares arbitrary intake to arbitrary profile"
      (should=
	  (make-intake 1782 426)
	(balance
	 (make-profile 'man 32 69 22)
	 (make-intake 12552 3000))))
    (it "compares arbitraty intake to arbitrary profile other gender other needs"
      (should=
	  (make-intake 3954 945)
	(balance
	 (make-profile 'woman 33 69 22)
	 (make-intake 12552 3000))))))
