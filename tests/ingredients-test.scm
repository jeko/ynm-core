(define-module (tests harness-ingredients)
  #:use-module (spec)
  #:use-module (tests utils)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients)
  #:use-module (ynm-core adapters))

(install-spec-runner-repl)

(define-public (run-tests-ingredients-list)
  (describe "ingredient list"
    (it "has no ingredient"
      (should=
	  '()
	(let ([food-base (build-calnut-base CALNUT_TABLE_NO_ALIM)])
	  (ingredients food-base))))
    (it "has one ingredient"
      (should=
	  (list
	   (make-food
	    40601
	    "Abat, cuit (aliment moyen)"
	    "MB"
	    (make-intake 682 162)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_ONE_ALIM_1)])
	  (ingredients food-base))))
    (it "has one ingredient bis"
      (should=
	  (list
	   (make-food
	    12112
	    "Abondance"
	    "MB"
	    (make-intake 1640 395)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_ONE_ALIM_2)])
	  (ingredients food-base))))
    (it "has two ingredients"
      (should=
	  (list
	   (make-food
	    40601
	    "Abat, cuit (aliment moyen)"
	    "MB"
	    (make-intake 682 162))
	   (make-food
	    12112
	    "Abondance"
	    "MB"
	    (make-intake 1640 395)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base))))))

(define-public (run-tests-ingredients-search)
  (describe "ingredient search"
    (it "searches by food_label exact label"
      (should=
	  (list
	   (make-food
	    12112
	    "Abondance"
	    "MB"
	    (make-intake 1640 395)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base #:label "Abondance"))))
    (it "searches by food_label exact label bis"
      (should=
	  (list
	   (make-food
	    40601
	    "Abat, cuit (aliment moyen)"
	    "MB"
	    (make-intake 682 162)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base #:label "Abat, cuit (aliment moyen)"))))
    (it "searches by food_label unknown food"
      (should=
	  '()
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base #:label "Ingrédient inconnu"))))
    (it "searches by food_label partial label"
      (should=
	  (list
	   (make-food
	    40601
	    "Abat, cuit (aliment moyen)"
	    "MB"
	    (make-intake 682 162))
	   (make-food
	    12112
	    "Abondance"
	    "MB"
	    (make-intake 1640 395)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base #:label "Ab"))))
    (it "searches by food_label partial label case insensitive"
      (should=
	  (list
	   (make-food
	    40601
	    "Abat, cuit (aliment moyen)"
	    "MB"
	    (make-intake 682 162)))
	(let ([food-base (build-calnut-base CALNUT_TABLE_TWO_ALIMS)])
	  (ingredients food-base #:label "abat"))))))
