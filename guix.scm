(use-modules
 (jeko-packages)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages code)
 (guix packages)
 (guix build-system guile)
 (guix build utils)
 (guix licenses)
 (guix git-download)
 (guix gexp)
 (ice-9 popen)
 (ice-9 rdelim))

(define source-dir
  (dirname (current-filename)))
(define git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))
(define revision
  "0")

(package
 (inherit ynm-core)
 (name "ynm-core-git")
 (version (string-append (package-version ynm-core) "-HEAD"))
 (source
  (local-file
   source-dir
   #:recursive? #t
   ;;#:select? (not (basename ".git"))
   )))
